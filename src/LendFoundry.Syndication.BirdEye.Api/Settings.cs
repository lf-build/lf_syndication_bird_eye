﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Syndication.BirdEye.Api
{
    public class Settings
    {
        public static string ServiceName => "bird-eye";

        private static string Prefix = "BIRDEYE";

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Lookup { get; } = new ServiceSettings($"{Prefix}_LOOKUP", "lookup");

        public static string Nats { get; } = Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";
        public static ServiceSettings TlsProxy { get; } = new ServiceSettings($"{Prefix}_TLSPROXY", "tlsproxy");
    }
}