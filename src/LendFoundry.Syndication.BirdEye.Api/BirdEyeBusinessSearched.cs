﻿using LendFoundry.Syndication.BirdEye.Response;

namespace LendFoundry.Syndication.BirdEye.Api
{
    public class BirdEyeBusinessSearched
    {
        public BirdEyeBusinessSearched()
        {
        }

        public BirdEyeBusinessSearched(ISearchBusinessResponse searchBusinessResponse)
        {
            SearchBusinessResponse = searchBusinessResponse;
        }

        public ISearchBusinessResponse SearchBusinessResponse { get; set; }
    }
}