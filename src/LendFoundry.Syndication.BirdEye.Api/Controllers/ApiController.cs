﻿using LendFoundry.EventHub.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.BirdEye.Request;
using LendFoundry.SyndicationStore.Events;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.BirdEye.Api.Controllers
{
    [Route("/")]
    public class ApiController : ExtendedController
    {
        public ApiController(IBirdEyeService service, ILogger logger, IEventHubClient eventHubClient) : base(logger)
        {
            Service = service;
            EventHubClient = eventHubClient;
        }

        private IBirdEyeService Service { get; }

        private IEventHubClient EventHubClient { get; }

        [HttpPost("{entityType}/{entityId}/search")]
        public async Task<IActionResult> Search([FromRoute] string entityType, [FromRoute] string entityId, [FromBody] SearchBusinessRequest request)
        {
            return await ExecuteAsync(async () =>
           {
               try
               {
                   var searchBusinessResponse = await Service.Search(entityType, entityId, request);

                   await EventHubClient.Publish(nameof(SyndicationCalledEvent), new SyndicationCalledEvent
                   {
                       Data = searchBusinessResponse,
                       EntityId = entityId,
                       EntityType = entityType,
                       Name = "BirdEye"
                   });

                   await EventHubClient.Publish(new BirdEyeBusinessSearched(searchBusinessResponse));

                   return new OkObjectResult(searchBusinessResponse);
               }
               catch (BirdEyeException ex)
               {
                   return BadRequest(new Error(ex.Message));
                  // return BadRequest(ex.Message);
               }
               catch (Exception ex)
               {
                   if (ex is ArgumentNullException || ex is ArgumentException)
                   {
                       Logger.Error(ex.Message);
                       throw;
                   }

                   var message = ex.Message;
                   var exception = ex.InnerException as BirdEyeException;

                   if (exception != null)
                       message = exception.Message;

                   Logger.Error($"The method Search({entityType},{entityId}) raised an error: {message}", ex);
                   throw new NotFoundException($"Information not found for entity type :{entityType} with {entityId}");
               }
           });
        }

        private static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s)) return string.Empty;
            return $"{char.ToUpper(s[0])}{s.Substring(1)}";
        }
    }
}