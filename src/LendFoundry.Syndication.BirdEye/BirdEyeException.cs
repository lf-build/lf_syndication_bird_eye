﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.BirdEye
{
    [Serializable]
    public class BirdEyeException : Exception
    {
        public string ErrorCode { get; set; }

        public BirdEyeException()
        {
        }

        public BirdEyeException(string message) : this(null, message, null)
        {
        }

        public BirdEyeException(string errorCode, string message) : this(errorCode, message, null)
        {
        }

        public BirdEyeException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }

        public BirdEyeException(string message, Exception innerException) : this(null, message, innerException)
        {
        }

        protected BirdEyeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}