﻿namespace LendFoundry.Syndication.BirdEye
{
    public interface IBirdEyeConfiguration
    {
        string BaseUrl { get; set; }
        string ApiKey { get; set; }

        string ProxyUrl { get; set; }
    }
}