﻿using LendFoundry.Syndication.BirdEye.Request;
using LendFoundry.Syndication.BirdEye.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.BirdEye
{
    public interface IBirdEyeService
    {
        Task<ISearchBusinessResponse> Search(string entityType, string entityId, ISearchBusinessRequest request);
    }
}