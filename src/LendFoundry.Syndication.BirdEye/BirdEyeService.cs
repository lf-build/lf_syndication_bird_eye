﻿using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.BirdEye.Proxy;
using LendFoundry.Syndication.BirdEye.Request;
using LendFoundry.Syndication.BirdEye.Response;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.BirdEye
{
    public class BirdEyeService : IBirdEyeService
    {
        public BirdEyeService(IBirdEyeProxy birdEyeProxy, IBirdEyeConfiguration configuration, ILookupService lookup)
        {
            if (birdEyeProxy == null)
                throw new ArgumentNullException(nameof(birdEyeProxy));
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (lookup == null)
                throw new ArgumentNullException(nameof(lookup));
            BirdEyeProxy = birdEyeProxy;
            Configuration = configuration;
            Lookup = lookup;
        }

        private IBirdEyeProxy BirdEyeProxy { get; }
        private IBirdEyeConfiguration Configuration { get; }
        private ILookupService Lookup { get; }

        public Task<ISearchBusinessResponse> Search(string entityType, string entityId, ISearchBusinessRequest request)
        {
            EnsureEntityId(entityId);
            entityType = EnsureEntityType(entityType);
            EnsureSearchBusiness(request);
            var searchBusinessRequest = new Proxy.Request.SearchBusinessRequest
            {
                BusinessName = request.BusinessName,
                ZipCode = request.ZipCode
            };
            var searchBusinessResult = BirdEyeProxy.SearchBusiness(searchBusinessRequest);

            return Task.Run<ISearchBusinessResponse>(() => doReturnSearchResult(entityType, entityId, searchBusinessResult));
        }

        private ISearchBusinessResponse doReturnSearchResult(string entityType, string entityId, Proxy.Response.SearchBusinessResult searchBusinessResult)
        {
            if (searchBusinessResult == null)
                return null;
            return new SearchBusinessResponse(entityType, entityId, searchBusinessResult);
        }

        private void EnsureSearchBusiness(ISearchBusinessRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (string.IsNullOrEmpty(request.BusinessName))
                throw new ArgumentException("Business name is required", nameof(request.BusinessName));
            if (string.IsNullOrEmpty(request.ZipCode))
                throw new ArgumentException("Zip code is required", nameof(request.ZipCode));
        }

        private void EnsureEntityId(string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("EntityId is required.");
        }

        private string EnsureEntityType(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new InvalidArgumentException("Invalid Entity Type");

            entityType = entityType.ToLower();
            var validEntityTypes = Lookup.GetLookupEntry("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any())
                throw new InvalidArgumentException("Invalid Entity Type");

            return entityType;
        }
    }
}