﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.BirdEye.Proxy.Response
{
    public class SearchBusinessResult
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "category")]
        public string Category { get; set; }

        [JsonProperty(PropertyName = "address")]
        public Address Address { get; set; }

        [JsonProperty(PropertyName = "reviewCount")]
        public int ReviewCount { get; set; }

        [JsonProperty(PropertyName = "avgRating")]
        public double AvgRating { get; set; }

        [JsonProperty(PropertyName = "profileURL")]
        public string ProfileURL { get; set; }

        [JsonProperty(PropertyName = "coverImageURL")]
        public string CoverImageURL { get; set; }

        [JsonProperty(PropertyName = "logoURL")]
        public string LogoURL { get; set; }

        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }

        [JsonProperty(PropertyName = "businessId")]
        public string BusinessId { get; set; }

        [JsonProperty(PropertyName = "websiteUrl")]
        public string WebsiteUrl { get; set; }

        [JsonProperty(PropertyName = "reviewComment")]
        public string ReviewComment { get; set; }

        [JsonProperty(PropertyName = "emailId")]
        public string EmailId { get; set; }

        [JsonProperty(PropertyName = "contactInfoList")]
        public object ContactInfo { get; set; }
    }

    public class Address
    {
        [JsonProperty(PropertyName = "address1")]
        public string Address1 { get; set; }

        [JsonProperty(PropertyName = "address2")]
        public string Address2 { get; set; }

        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }

        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }

        [JsonProperty(PropertyName = "zip")]
        public string Zip { get; set; }

        [JsonProperty(PropertyName = "countryCode")]
        public string CountryCode { get; set; }

        [JsonProperty(PropertyName = "countryName")]
        public string CountryName { get; set; }
    }
}