﻿using LendFoundry.Syndication.BirdEye.Proxy.Response;

namespace LendFoundry.Syndication.BirdEye.Proxy
{
    public interface IBirdEyeProxy
    {
        SearchBusinessResult SearchBusiness(Request.SearchBusinessRequest searchBusinessRequest);
    }
}