﻿namespace LendFoundry.Syndication.BirdEye.Proxy.Request
{
    public class SearchBusinessRequest
    {
        public string BusinessName { get; set; }
        public string ZipCode { get; set; }
    }
}