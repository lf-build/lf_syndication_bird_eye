﻿using LendFoundry.Syndication.BirdEye.Proxy.Response;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.BirdEye.Proxy
{
    public class BirdEyeProxy : IBirdEyeProxy
    {
        public BirdEyeProxy(IBirdEyeConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));
            if (string.IsNullOrEmpty(configuration.BaseUrl))
                throw new ArgumentNullException(nameof(configuration.BaseUrl));
            if (string.IsNullOrEmpty(configuration.ApiKey))
                throw new ArgumentNullException(nameof(configuration.ApiKey));

            if (string.IsNullOrEmpty(configuration.ProxyUrl))
                throw new ArgumentNullException(nameof(configuration.ProxyUrl));
            Configuration = new BirdEyeConfiguration(configuration);
        }

        public IBirdEyeConfiguration Configuration { get; }

        public SearchBusinessResult SearchBusiness(Request.SearchBusinessRequest searchBusinessRequest)
        {
            if (searchBusinessRequest == null)
                throw new ArgumentNullException(nameof(searchBusinessRequest));
            string urlWithQuery = $"{Configuration.BaseUrl}/business/search";

            var baseUri = new Uri(urlWithQuery);

            var restRequest = new RestRequest(Method.GET);
            restRequest.AddHeader("Content-Type", "application/json");
            restRequest.AddHeader("Accept", "application/json");
            var uri = new Uri($"{Configuration.ProxyUrl}{baseUri.PathAndQuery}");
            RestClient client = new RestClient(uri);
            restRequest.AddHeader("Host", baseUri.Host);
            restRequest.AddQueryParameter("api_key", Configuration.ApiKey);
            if (!string.IsNullOrEmpty(searchBusinessRequest.BusinessName))
                restRequest.AddQueryParameter("search_str", searchBusinessRequest.BusinessName);
            if (!string.IsNullOrEmpty(searchBusinessRequest.ZipCode))
                restRequest.AddQueryParameter("loc", searchBusinessRequest.ZipCode);

            var objectResponse = ExecuteRequest<IEnumerable<SearchBusinessResult>>(client, restRequest);
            SearchBusinessResult searchBusinessResult = null;
            if (objectResponse != null && objectResponse.Count() > 0)
                searchBusinessResult = objectResponse.FirstOrDefault();

            return searchBusinessResult;
        }

        private static T ExecuteRequest<T>(IRestClient client, IRestRequest request) where T : class
        {
            var response = client.Execute(request);

            if (response == null)
                throw new ArgumentNullException(nameof(response));

            if (response.ErrorException != null)
                throw new BirdEyeException("Service call failed", response.ErrorException);

            if (response.ResponseStatus != ResponseStatus.Completed)
                throw new BirdEyeException(
                    $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if ((response.StatusCode.ToString().ToLower() == "internalservererror") || (response.Content.ToLower().Contains("error message:")))
                    throw new BirdEyeException(
                      $"Service call failed. Status {response.ResponseStatus}. Response: {response.Content ?? ""}");
            }
            return JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}