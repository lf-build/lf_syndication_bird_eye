﻿namespace LendFoundry.Syndication.BirdEye.Response
{
    public class SearchBusinessResponse : ISearchBusinessResponse
    {
        public SearchBusinessResponse()
        {
        }

        public SearchBusinessResponse(string entityType, string entityId, Proxy.Response.SearchBusinessResult searchBusinessResult)
        {
            if (searchBusinessResult != null)
            {
                EntityType = entityType;
                EntityId = entityId;
                ReviewCount = searchBusinessResult.ReviewCount;
                AvgRating = searchBusinessResult.AvgRating;
            }
        }

        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public double ReviewCount { get; set; }
        public double AvgRating { get; set; }
    }
}