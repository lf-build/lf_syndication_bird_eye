﻿namespace LendFoundry.Syndication.BirdEye.Response
{
    public interface ISearchBusinessResponse
    {
        string EntityType { get; set; }
        string EntityId { get; set; }
        double ReviewCount { get; set; }
        double AvgRating { get; set; }
    }
}