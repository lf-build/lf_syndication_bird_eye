﻿namespace LendFoundry.Syndication.BirdEye.Request
{
    public interface ISearchBusinessRequest
    {
        string BusinessName { get; set; }
        string ZipCode { get; set; }
    }
}