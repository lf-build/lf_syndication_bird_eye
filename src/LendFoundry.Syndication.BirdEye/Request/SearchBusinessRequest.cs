﻿namespace LendFoundry.Syndication.BirdEye.Request
{
    public class SearchBusinessRequest : ISearchBusinessRequest
    {
        public string BusinessName { get; set; }
        public string ZipCode { get; set; }
    }
}