﻿namespace LendFoundry.Syndication.BirdEye
{
    public class BirdEyeConfiguration : IBirdEyeConfiguration
    {
        public BirdEyeConfiguration()
        { }

        public BirdEyeConfiguration(IBirdEyeConfiguration configuration)
        {
            if (configuration == null)
                return;
            BaseUrl = configuration.BaseUrl;
            ApiKey = configuration.ApiKey;

            ProxyUrl = configuration.ProxyUrl;
        }

        public string BaseUrl { get; set; }
        public string ApiKey { get; set; }

        public string ProxyUrl { get; set; }
    }
}